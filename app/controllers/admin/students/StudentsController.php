<?php namespace Controllers\Admin\Students;

use AdminController;
use View;
use Input;
use Redirect;
use Validator;
use Student;
use Debugbar;

class StudentsController extends AdminController {

	/**
	 * Show the administration dashboard page.
	 *
	 * @return View
	 */

	public function getView()
	{
		if(Input::has('q')){
			try {
				$data = Student::where('r_id', '=', Input::get('q'))->firstOrFail();

				return Redirect::route('students.view', $data->id);
			}
			catch(\Exception $e){
				Debugbar::addException($e);
				$data = Student::where('name', 'like', '%'.Input::get('q').'%')->paginate(25);
				dd($data);
			}
		}
		else {
			$data = Student::paginate(25);
		}

		return View::make('backend/students/viewStudents')->with('msg', $data);
	}

	public function getSingle($id){
		try {
			$student = Student::findOrFail($id);

			return View::make('backend/students/single')->with('student', $student);
		}
		catch(\Exception $e){
			Debugbar::addException($e);

			return Redirect::back()->with('error', 'Student not found');
		}
	}

	public function getAdd()
	{
		// Show the page
		return View::make('backend/students/addStudents');
	}

	public function postAdd()
	{
		$input=Input::all();

		$validation=Validator::make($input,array(
			'roll_number'=>'required|alpha_num|unique:students,r_id',
			'name'=>'required',
			'education'=>'required',
			'category'=>'required',
			'course'=>'required',
			'centre'=>'required',
			'doj'=>'required',
			'photo' => 'image',
			'email' => 'required|email',
			'phone_no' => 'min:8|max:15',
			'dob' => 'required',
		));

		if($validation->passes())
		{
			$student=new Student();
			$student->r_id= Input::get('roll_number');
			$student->biometric_no = Input::get('biometric_no');
			$student->name= Input::get('name');
			$student->education= Input::get('education');
			$student->gender= Input::get('gender');
			$student->category= Input::get('category');
			$student->course_id= Input::get('course');
			$student->centre_id= Input::get('centre');
			$student->joined_at = \DateTime::createFromFormat('!Y-m-d', Input::get('doj'));

			$student->photo = Input::file('photo', null);
			$student->email = Input::get('email');
			$student->phone_no = Input::get('phone_no');
			$student->address = Input::get('address');
			$student->dob = \DateTime::createFromFormat('!Y-m-d', Input::get('dob'));
			$student->batch_no = Input::get('batch_no');

			$student->remark= Input::get('remark');

			$student->save();
			return Redirect::to('admin/students')->withSuccess('Student #'.$student->r_id.' has been added.');
		}
		else
		{
			return Redirect::back()->withInput()->withErrors($validation);
		}
	}

	public function getUpdate($id)
	{
		$data=Student::find($id);
		return View::make('backend/students/updateStudents')->with('data',$data);
	}

	public function postUpdate($id)
	{
		$input=Input::all();

		$validation=Validator::make($input,array(
			'roll_number'=>'required|alpha_num|unique:students,r_id,'.$id,
			'name'=>'required',
			'education'=>'required',
			'category'=>'required',
			'course'=>'required',
			'centre'=>'required',
			'doj'=>'required',
			'photo' => 'image',
			'email' => 'required|email',
			'phone_no' => 'min:8|max:15',
			'dob' => 'required',
		));

		if($validation->passes())
		{
			$student=Student::find($id);
			$student->r_id= Input::get('roll_number');
			$student->biometric_no = Input::get('biometric_no');
			$student->name= Input::get('name');
			$student->education= Input::get('education');
			$student->gender= Input::get('gender');
			$student->category= Input::get('category');
			$student->course_id= Input::get('course');
			$student->centre_id= Input::get('centre');
			$student->joined_at= \DateTime::createFromFormat('!Y-m-d', Input::get('doj'));

			$student->photo = Input::file('photo', null);
			$student->email = Input::get('email');
			$student->phone_no = Input::get('phone_no');
			$student->address = Input::get('address');
			$student->dob = \DateTime::createFromFormat('!Y-m-d', Input::get('dob'));
			$student->batch_no = Input::get('batch_no');

			$student->remark= Input::get('remark');

			$student->save();
			return Redirect::to('admin/students')->with('success','Data has been successfully updated');
		}
		else
		{
			return Redirect::back()->withInput()->withErrors($validation);
		}
	}

	public function getTrack($id){
		try {
			$student = Student::findOrFail($id);

			if($student->student_id != null){
				// This student can not be tracked
				return Redirect::back()->with('error', 'Invalid student to track');
			}

			return View::make('backend/students/track')->with('student', $student);
		}
		catch(\Exception $e){
			Debugbar::addException($e);

			return Redirect::back()->with('error', 'Student not found');
		}
	}

	public function postTrack($id){
		try {
			$student = Student::findOrFail($id);

			if($student->student_id != null){
				// This student can not be tracked
				return Redirect::back()->with('error', 'Invalid student to track');
			}

			$rules = array(
				'photo' => 'image',
				'email' => 'email',
			);

			$validation = Validator::make(Input::all(), $rules);

			if($validation->fails()){
				return Redirect::back()->withErrors($validation)->withInput();
				// Backing up, something wrong with inputs.
			}

			$revision = new Student;

			$revision->student_id = $student->id;
			$revision->photo = Input::file('photo');
			$revision->address = Input::get('address');
			$revision->email = Input::get('email');
			$revision->phone_no = Input::get('phone_no');
			$revision->company_id = Input::get('company_id');
			$revision->designation = Input::get('designation');
			$revision->salary = Input::get('salary');

			$revision->save();

			return Redirect::route('students.view', $id)->with('success', 'Tracking information added.');
		}
		catch(\Exception $e){
			Debugbar::addException($e);

			return Redirect::back()->with('error', 'Student not found');
		}
	}

	public function postDelete()
	{
		$data=Input::get('delete');
		
		foreach($data as $id)
		{
			 //dd($id);
			 $row=Student::find($id);
			 $row->delete();			
		}
		return Redirect::back();
	}

}

