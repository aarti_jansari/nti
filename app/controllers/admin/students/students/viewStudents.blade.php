@extends('backend/layouts/default')

@section('content')
<div class="row">
	<div class="col-md-12">
		<form method="post" action="{{ URL::to('admin/students') }}">
			<table class="table">
				<thead>
				<tr>
					<th>Delete</th>
					<th>Roll Number</th>
					<th>Name</th>
					<th>Education</th>
					<th>Gender</th>
					<th>Category</th>
					<th>Course</th>
					<th>Centre</th>
					<th>Date of Joining</th>
					<th>Remarks</th>
					<th class="col-md-1" align="right">Update</th>
				</tr>
				</thead>
			@foreach($msg as $value)
			
				<tr>
					<td><input type='checkbox' name='delete[]' value="{{ $value->id }}"></td>
					<td>{{ $value->r_id }}</td>
					<td>{{ $value->name}}</td>
					<td>{{ $value->education }}</td>
					<td>{{ $value->gender}}</td>
					<td>{{ $value->category}}</td>
					<td>{{ $value->course_id}}</td>
					<td>{{ $value->center_id}}</td>
					<td>{{ $value->joined_at}}</td>
					<td>{{ $value->remark}}</td>
					<td align="right"><a class="btn btn-primary btn-sm" href="{{ URL::to('admin/students/update/'.$value->id) }}" >Edit</a></td></tr>
			@endforeach
			</table>
			<button type='submit' class="btn btn-danger pull-left">Delete</button>
			<a href="{{ URL::to('admin/students/add') }}" type='submit' class="btn btn-success pull-right col-md-1">Add New</a>
			
		</form>
	</div>
</div>
@stop