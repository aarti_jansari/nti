<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePhoneNoInCentresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('centres', function(Blueprint $table){
			$table->dropColumn('phone_no');
		});

		Schema::table('centres', function(Blueprint $table){
			$table->string('phone_no', 15)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('centres', function(Blueprint $table){
			$table->dropColumn('phone_no');
		});

		Schema::table('centres', function(Blueprint $table){
			$table->integer('phone_no');
		});
	}

}
