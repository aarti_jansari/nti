<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('centres',function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('place');
			$table->string('district');
			$table->text('address');
			$table->integer('phone_no');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('centres');
	}

}
