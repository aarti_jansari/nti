<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJoinedAtDatatypeInStudents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('students', function(Blueprint $table){
			$table->dropColumn('joined_at');
		});

		Schema::table('students', function(Blueprint $table){
			$table->date('joined_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('students', function(Blueprint $table){
			$table->dropColumn('joined_at');
		});

		Schema::table('students', function(Blueprint $table){
			$table->datetime('joined_at')->nullable();
		});
	}

}
