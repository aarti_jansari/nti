<?php

class Course extends Eloquent {
	public function students(){
		return $this->hasMany('Student');
	}
}