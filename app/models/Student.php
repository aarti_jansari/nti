<?php

class Student extends Eloquent {
	public static function boot(){
		parent::boot();

		Student::saving(function($model){
			if($model->photo != null && $model->photo instanceOf File){
				$fileName = time().'.'.$model->photo->getClientOriginalExtension();
	
				if($model->photo->move(public_path(Config::get('app.upload_path')), $fileName)){
					$model->photo = $fileName;
					return true;
				}

				return false;				
			}

			unset($model->photo);

			// Copy revisions data
			/*if($model->student_id != null){
				try {
					$student = Student::findOrFail($model->student_id);

					foreach($student->attributes as $attribute => $value){
						Debugbar::addDebug($attribute.' '.$value);
					}

					return false;
				}
				catch(Exception $e){
					Debugbar::addException($e);
					return false;
				}
			}*/

			return true;
		});

	}

	public function centre(){
		return $this->belongsTo('Centres');
	}

	public function course(){
		return $this->belongsTo('Course');
	}

	public function company(){
		return $this->belongsTo('Company');
	}

	public function revisions(){
		return $this->hasMany('Student', 'student_id', 'id');
	}

	public function actual(){
		return $this->belongsTo('Student', 'student_id', 'id');
	}

	public function getGenderAttribute($value){
		return ($value == 0) ? 'Male' : 'Female';
	}

	public function getCategoryAttribute($value)
	{
		return strtoupper($value);
	}
	public function getJoinedAtAttribute($value)
	{
		//return date_format(dd-mm-yyyy)
		return date("d-m-Y", strtotime($value));
	}

	public function getPhotoAttribute($value){
		return url(Config::get('app.upload_path').'/'.$value);
	}
}
?>