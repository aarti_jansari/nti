@extends('frontend/layouts/default')

@section('title')
NTI Student Tracking System
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				<h4 class="pull-left">Filtered Data</h4>
				<a href="{{ URL::to('/') }}" class="btn btn-info pull-right">Filter</a>
			</div>
				<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th>Roll Number</th>
							<th>Name</th>
							<th>Gender</th>
							<th>Category</th>
							<th>Centre</th>
							<th>Course</th>						
							<th>Batch #</th>
							<th>Joined At</th>
							<th>View</th>						
						</tr>
					</thead>
					@foreach($msg as $value)			
						<tr>
							<td>{{$value->r_id}}</td>
							<td>{{$value->name}}</td>
							<td>{{$value->gender}}</td>
							<td>{{$value->category}}</td>
							<td>{{$value->centre->name}}</td>
							<td>{{$value->course->name}}</td>
							<td>{{$value->batch_no}}</td>
							<td>{{$value->joined_at}}</td>
							<td align="right"><a class="btn btn-primary btn-sm" href="{{ route('home.view', $value->r_id) }}" >View</a></td></tr>
						</tr>
					@endforeach
				</table>
				</div>
				</div>		
				<div class="panel-footer clearfix">
					<div class="pull-right">
						<nav>
				  			<ul class="pagination">
  								<?php echo with(new BootstrapPaginatorPresenter($msg))->render(); ?>
  							</ul>
						</nav>
					</div>
				</div>
			</div>
	</div>
</div>
@stop
