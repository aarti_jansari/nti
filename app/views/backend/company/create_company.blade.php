@extends('backend/layouts/default')
@section('title')
Company Detail
@parent
@stop


@section('content')
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
		<form class="form-horizontal" action="{{ route('create.company') }}" method="post">
			<div class="panel panel-default">
				<div class="panel-heading text-center">Enter the Company Detail</div>
				<div class="panel-body">	
	
				<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label class="col-md-3 control-label" for="name">Name:</label>
							<div class="col-md-9">	
								<input class="form-control" type="text" name="name" id="name" value="{{ Input::old('name') }}" >
								{{ $errors->first('name', '<span class="help-block">:message</span>') }}
							</div>
				</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="address">Address:</label>
						<div class="col-md-9">	
						<textarea class="form-control" name="address" id="address" value="{{ Input::old('address') }}" ></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="remark">Remark:</label>
						<div class="col-md-9">	
						<textarea class="form-control" name="remark" id="remark" value="{{ Input::old('remark') }}" ></textarea>
						</div>
					</div>
				</div>
					<div class="panel-footer text-center clearfix">
						<input type="submit" class="btn btn-success">
						<input type="button" value="Cancel" class="btn btn-danger">
					</div>
				</div>
			</form>
@stop