@extends('backend/layouts/default')

@section('content')
<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<form class="form-horizontal" method="post" action="{{ URL::to('admin/companies/update/'.$data->id) }}">
		<div class="panel panel-default">
			<div class="panel-heading">Updating: {{ $data->name }}</div>
			<div class="panel-body">
					
					<div class="form-group {{ (($errors->has('name')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="name">Name:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="name" value="{{ Input::old('name', $data->name) }}">
							<?php echo $errors->first('name', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="address">address:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="address" value="{{ Input::old('address', $data->address) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="remark">Remark:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="remark" value="{{ Input::old('remark', $data->remark) }}">
						</div>
					</div>


			</div>
			<div class="panel-footer clearfix">
				<button type="submit" name="submit" class="btn btn-success col-md-5">Update</button>
				<a href="{{ url('admin/companies') }}" class="btn btn-danger col-md-offset-2 col-md-5">Cancel</a>
			</div>
		</div>
		</form>
	</div>
</div>
@stop