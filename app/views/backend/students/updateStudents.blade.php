@extends('backend/layouts/default')

@section('content')
<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<form class="form-horizontal" method="post" action="<?php echo URL::to('admin/students/update/'.$data->id); ?>" enctype="multipart/form-data">
		<div class="panel panel-default">
			<div class="panel-heading">Updating: {{ $data->name }}</div>
			<div class="panel-body">
					
					<div class="form-group {{ (($errors->has('roll_number')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="roll_number">Roll Number:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="roll_number" value="{{ Input::old('roll_number', $data->r_id) }}">
							<?php echo $errors->first('roll_number', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('biometric_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="biometric_no">Biometric Number:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="biometric_no" value="{{ Input::old('biometric_no', $data->biometric_no) }}">
							<?php echo $errors->first('biometric_no', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('name')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="name">Name:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="name" value="{{ Input::old('name', $data->name) }}">
							<?php echo $errors->first('name', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('education')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="name">Education:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="education" value="{{ Input::old('education', $data->education) }}">
							<?php echo $errors->first('education', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('gender')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="gender">Gender:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="gender" value="{{ Input::old('gender', $data->gender) }}">
							<?php echo $errors->first('gender', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('category')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="category">Category:</label>
						<div class="col-md-9">
							{{ Form::casts('category', Input::old('category', $data->category)) }}
							<?php echo $errors->first('category', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>	

					<div class="form-group {{ (($errors->has('course')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="course">Course:</label>
						<div class="col-md-9">
							{{ Form::dropdown('course', Course::all()->lists('name', 'id'), Input::old('course', $data->course_id)) }}
							<?php echo $errors->first('course', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('centre')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="centre">Centre:</label>
						<div class="col-md-9">
							{{ Form::dropdown('centre', Centres::all()->lists('name', 'id'), Input::old('centre', $data->centre_id)) }}
							<?php echo $errors->first('centre', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('doj')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="doj">Joined At:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="doj" name="doj" value="{{ Input::old('doj', $data->joined_at) }}">
							<?php echo $errors->first('doj', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('photo')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="doj">Photo:</label>
						<div class="col-md-9">	
							{{ Form::file('photo') }}
							{{$errors->first('photo','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('address')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="address">Address:</label>
						<div class="col-md-9">	
						<textarea class="form-control" type="text" name="address">{{ Input::old('address', $data->address) }}</textarea>
						{{$errors->first('address','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('dob')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="dob">Date of Birth:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="dob" id="dob" value="{{ Input::old('dob', $data->dob) }}">							
						{{$errors->first('dob','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('batch_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="batch_no">Batch Number:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="batch_no" value="{{ Input::old('batch_no', $data->batch_no) }}">
						{{$errors->first('batch_no','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('email')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="email">Email:</label>
						<div class="col-md-9">	
						<input class="form-control" type="email" name="email" value="{{ Input::old('email', $data->email) }}">
						{{$errors->first('email','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('phone_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="phone_no">Phone Number:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="phone_no" value="{{ Input::old('phone_no', $data->phone_no) }}">
						{{$errors->first('phone_no','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('remark')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="remark">Remark:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="remark" value="{{ Input::old('remark', $data->remark) }}">
							<?php echo $errors->first('remark', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>


			</div>
			<div class="panel-footer clearfix">
				<button type="submit" name="sbmt" class="btn btn-success col-md-5">Update</button>
				<a href="{{ url('admin/students') }}" class="btn btn-danger col-md-offset-2 col-md-5">Cancel</a>
			</div>
		</div>
		</form>
	</div>
</div>
@stop

@section('footer')
<script type="text/javascript">
$(function(){
	$("#doj").datepicker({
		dateFormat: 'yy-mm-dd'
	});
	$("#dob").datepicker({
		dateFormat: 'yy-mm-dd'
	});
});
</script>
@stop