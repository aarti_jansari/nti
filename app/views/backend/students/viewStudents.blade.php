@extends('backend/layouts/default')

@section('content')
<div class="row">
	<div class="col-md-12">
		<form method="post" action="{{ URL::to('admin/students') }}">
			<table class="table">
				<thead>
				<tr>
					<th>Delete</th>
					<th>Photo</th>
					<th>Roll Number</th>
					<th>Name</th>
					<th>Centre</th>
					<th>Course</th>
					<th>Batch #</th>
					<th class="col-md-1" align="right">Update</th>
				</tr>
				</thead>
			@foreach($msg as $value)
			
				<tr>
					<td><input type='checkbox' name='delete[]' value="{{ $value->id }}"></td>
					<td><img src="{{ $value->photo }}" class="img-responsive" width="150" /></td>
					<td>{{ $value->r_id }}</td>
					<td><a href="{{ route('students.view', $value->id) }}">{{ $value->name}}</a></td>
					<td>{{ $value->centre->name }}</td>
					<td>{{ $value->course->name }}</td>
					<td>{{ $value->batch_no }}</td>
					<td align="right"><a class="btn btn-primary btn-sm" href="{{ URL::to('admin/students/update/'.$value->id) }}" >Edit</a></td></tr>
			@endforeach
			</table>
			<button type='submit' class="btn btn-danger pull-left">Delete</button>
			<a href="{{ URL::to('admin/students/add') }}" type='submit' class="btn btn-success pull-right col-md-1">Add New</a>
			
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<nav>
  			<ul class="pagination">
  				<?php echo with(new BootstrapPaginatorPresenter($msg))->render(); ?>
  			</ul>
		</nav>
	</div>
</div>
@stop