@extends('backend/layouts/default')

@section('title')
{{ $student->name }}
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<h4 class="pull-left">{{ $student->name }}</h4>
				<a href="{{ route('students.track', $student->id) }}" class="btn btn-info pull-right">Track</a>
			</div>
			<div class="panel-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Information</a></li>
					<li role="presentation"><a href="#tracking" aria-controls="tracking" role="tab" data-toggle="tab">Trackings</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="info">
						<div class="row">
							<div class="col-md-9">
								<h4>Personal Information</h4>
								<hr/>
								<div class="row">
									<div class="col-md-3"><strong>Full Name: </strong></div>
									<div class="col-md-9">{{ $student->name }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Gender: </strong></div>
									<div class="col-md-9">{{ $student->gender }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Cast Category: </strong></div>
									<div class="col-md-9">{{ $student->category }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Address: </strong></div>
									<div class="col-md-9">{{ $student->address }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Phone Number: </strong></div>
									<div class="col-md-9">{{ $student->phone_no }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Email Address: </strong></div>
									<div class="col-md-9">{{ $student->email }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Date of Birth: </strong></div>
									<div class="col-md-9">{{ DateTime::createFromFormat('!Y-m-d', $student->dob)->format('d M, Y') }}</div>
								</div>
								
								<hr/>
								<h4>Academic Information</h4>
								<hr/>
								<div class="row">
									<div class="col-md-3"><strong>Roll Number: </strong></div>
									<div class="col-md-9">{{ $student->r_id }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Biometric Number: </strong></div>
									<div class="col-md-9">{{ $student->biometric_no }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Date Of Joining: </strong></div>
									<div class="col-md-9">{{ DateTime::createFromFormat('!Y-m-d', $student->joined_at)->format('d M, Y') }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Centre: </strong></div>
									<div class="col-md-9">{{ $student->centre->name }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Course: </strong></div>
									<div class="col-md-9">{{ $student->course->Name }}</div>
								</div>
								<div class="row">
									<div class="col-md-3"><strong>Batch Code: </strong></div>
									<div class="col-md-9">{{ $student->batch_no }}</div>
								</div>
								<hr/>
								<h4>Remarks</h4>
								<hr/>
								<div class="row">
									<div class="col-md-12">
										{{ $student->remark }}
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<img src="{{ $student->photo }}" class="img-responsive" />
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tracking">
						<table class="table table-stripped">
							<thead>
								<tr>
									<th>Revised At</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone Number</th>
								</tr>
							</thead>
							<tbody>
								@foreach($student->revisions as $revision)
								<tr>
									<td>{{ $revision->created_at }}</td>
									<td>{{ $revision->name }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel-footer clearfix">
				<a href="{{ route('students.update', $student->id) }}" class="btn pull-left">Edit</a>
				<a href="{{ route('students.track', $student->id) }}" class="btn btn-info pull-right">Track</a>
			</div>
		</div>
	</div>
</div>
@stop