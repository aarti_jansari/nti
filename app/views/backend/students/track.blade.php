@extends('backend/layouts/default')

@section('title')
Track Entry for {{ $student->name }}
@stop

@section('content')
<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<form class="form-horizontal" method="post" action="<?php echo route('students.track', $student->id); ?>" enctype="multipart/form-data">
		<div class="panel panel-default">
			<div class="panel-heading">Tracking {{ $student->name }}</div>
			<div class="panel-body">
					
					<div class="form-group {{ (($errors->has('education')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="name">Education:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="education" value="{{ Input::old('education', $student->education) }}">
							<?php echo $errors->first('education', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>


					<div class="form-group {{ (($errors->has('photo')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="doj">Photo:</label>
						<div class="col-md-9">	
							{{ Form::file('photo') }}
							{{$errors->first('photo','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('address')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="address">Address:</label>
						<div class="col-md-9">	
						<textarea class="form-control" type="text" name="address">{{ Input::old('address', $student->address) }}</textarea>
						{{$errors->first('address','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('email')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="email">Email:</label>
						<div class="col-md-9">	
						<input class="form-control" type="email" name="email" value="{{ Input::old('email', $student->email) }}">
						{{$errors->first('email','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('phone_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="phone_no">Phone Number:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="phone_no" value="{{ Input::old('phone_no', $student->phone_no) }}">
						{{$errors->first('phone_no','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<hr/>
							<h4>Professional</h4>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('company_id')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="company_id">Company:</label>
						<div class="col-md-9">	
						{{ Form::dropdown('company_id', Company::all()->lists('name', 'id'), Input::old('company_id', $student->comapny)) }}
						{{$errors->first('company_id','<span class="help-block">:message</span>') }}
						</div>
					</div>					

					<div class="form-group {{ (($errors->has('designation')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="designation">Designation:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="designation" value="{{ Input::old('designation', $student->designation) }}">
						{{$errors->first('designation','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('salary')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="salary">Salary in Rs:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="salary" value="{{ Input::old('salary', $student->salary) }}">
						{{$errors->first('salary','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<hr/>
						</div>
					</div>


					<div class="form-group {{ (($errors->has('remark')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="remark">Remark:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="remark" value="{{ Input::old('remark', $student->remark) }}">
							<?php echo $errors->first('remark', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>


			</div>
			<div class="panel-footer clearfix">
				<button type="submit" name="sbmt" class="btn btn-success col-md-5">Add Track</button>
				<a href="{{ route('students.view', $student->id) }}" class="btn btn-danger col-md-offset-2 col-md-5">Cancel</a>
			</div>
		</div>
		</form>
	</div>
</div>
@stop

@section('footer')
<script type="text/javascript">
$(function(){
	$("#doj").datepicker({
		dateFormat: 'yy-mm-dd'
	});
	$("#dob").datepicker({
		dateFormat: 'yy-mm-dd'
	});
});
</script>
@stop