@extends('backend/layouts/default')

@section('content')
<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<form class="form-horizontal" method="post" action="{{ URL::to('admin/centres/update/'.$data->id) }}" enctype="multipart/form-data">
		<div class="panel panel-default">
			<div class="panel-heading">Updating: {{ $data->name }}</div>
			<div class="panel-body">
					<div class="form-group {{ (($errors->has('name')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="name">Name:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="name" value="{{ Input::old('name', $data->name) }}">
							<?php echo $errors->first('name', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>

					<div class="form-group {{ (($errors->has('place')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="place">Place:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="place" value="{{ Input::old('place', $data->place) }}">
							<?php echo $errors->first('place', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>
					<div class="form-group {{ (($errors->has('district')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="district">District:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="district" value="{{ Input::old('district', $data->district) }}">
							<?php echo $errors->first('district', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>
					<div class="form-group {{ (($errors->has('phone_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="phone_no">Phone No:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="phone_no" value="{{ Input::old('phone_no', $data->phone_no) }}">
							<?php echo $errors->first('phone_no', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>
					<div class="form-group {{ (($errors->has('address')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="address">Address:</label>
						<div class="col-md-9">
							<input class="form-control" type="text" name="address" value="{{ Input::old('address', $data->address) }}">
							<?php echo $errors->first('address', '<span class="help-block">:message</span>'); ?>
						</div>
					</div>


			</div>
			<div class="panel-footer clearfix">
				<button type="submit" name="sbmt" class="btn btn-success col-md-5">Update</button>
				<a href="{{ url('admin/centres') }}" class="btn btn-danger col-md-offset-2 col-md-5">Cancel</a>
			</div>
		</div>
		</form>
	</div>
</div>
@stop