@extends('backend/layouts/default')
@section('title')
Centre Detail
@parent
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<form method="post" action="{{ URL::to('admin/centres/delete') }}">
			<div class="panel panel-default">
				<div class="panel-heading text-center">Show All Centre Details</div>
				<div class="panel-body">	
			<table class="table">
				<thead>
				<tr>
					<th>Delete</th>
					<th>Id</th>
					<th>Name</th>
					<th>Place</th>
					<th>District</th>
					<th>Address</th>
					<th>Phone No</th>
					<th class="col-md-1" align="right">Update</th>
				</tr>
				</thead>
			@foreach($values as $value)
			
				<tr>
					<td><input type='checkbox' name='delete[]' value="{{ $value->id }}" /></td>
					<td>{{ $value->id }}</td>
					<td>{{ $value->name}}</td>
					<td>{{ $value->place }}</td>
					<td>{{ $value->district}}</td>
					<td>{{ $value->address}}</td>
					<td>{{ $value->phone_no}}</td>
					<td align="">
					<a class="panel-footer btn btn-primary btn-sm" href="{{ URL::to('admin/centres/update/'.$value->id) }}" >Edit</a></td>
				</tr>
			@endforeach
				</table>
					<button type='submit' class="panel-footer btn btn-danger pull-left">Delete</button>
						<a href="{{ route('create.centres') }}" type='submit' class="panel-footer btn btn-success pull-right">Add New</a>
					</div>
				</div>
		</form>
	</div>
</div>
@stop