@extends('backend/layouts/default')

@section('title')
Courses management
@stop

@section('content')
<div class="page-header">
	<h1>Courses Management</h1>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>All Courses</h4>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="col-md-2">Code</th>
							<th class="col-md-7">Name</th>
							<td class="col-md-3"></td>
						</tr>
						@foreach($courses as $course)
						<tr>
							<td>{{ $course->code }}</td>
							<td>{{ $course->name }}</td>
							<td>
								<a href="{{ route('courses.update', $course->id) }}" class="btn btn-info">Edit</a>
								<a href="{{ route('courses.delete', $course->id) }}" class="btn btn-danger">Delete</a>
							</td>
						</tr>
						@endforeach
					</thead>
				</table>		
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Add new Course</h4>
			</div>
			<div class="panel-body">
				{{ Template::form('add_course_form', route('courses.create'), array(
					'tabs' => array('General'),
					'tabsContent' => array(
						'General' => array(
							array(
								'name' => 'name',
								'label' => 'Name',
							),
							array(
								'name' => 'code',
								'label' => 'Code',
								'hint' => 'Must be unique'
							),
							array(
								'name' => 'description',
								'label' => 'Description',
								'field' => Form::textarea('description', Input::old('description'), array('class' => 'form-control')),
							),
						)
					)
				), $errors) }}
			</div>
		</div>
	</div>
</div>
@stop